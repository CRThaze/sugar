# sugar

[![GoDoc](http://godoc.org/gitlab.com/CRThaze/sugar?status.svg)](http://godoc.org/gitlab.com/CRThaze/sugar)

## Syntactic Sugar for Golang

This package provides some convenience functions that help keep code clean.
