package sugar

// StrSet a set type for strings.
type StrSet map[string]struct{}

// Add adds a new key to the set if it does not already exist.
func (s *StrSet) Add(str string) {
	(*s)[str] = struct{}{}
}

// Has checks if the given string is present in the set.
func (s *StrSet) Has(str string) bool {
	if _, ok := (*s)[str]; ok {
		return true
	}
	return false
}

// AddHas behaves like Add, but returns true if the key already existed.
func (s *StrSet) AddHas(str string) bool {
	if ok := s.Has(str); ok {
		return ok
	}
	s.Add(str)
	return false
}
